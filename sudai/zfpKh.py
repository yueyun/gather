import csv
import re

import time

from common.request import seleniumutil
from config import config


def to_search_page(browser):
    browser.switch_to.default_content()
    browser.find_element_by_css_selector("#tabs > div.tabs-header > div.tabs-wrap > ul > li.tabs-first").click()
    browser.switch_to.frame("iframeRightId")
    switch_selected_frame(browser)


def to_detail_page(browser):
    browser.switch_to.default_content()
    browser.find_element_by_css_selector("#tabs > div.tabs-header > div.tabs-wrap > ul > li.tabs-last").click()
    browser.switch_to.frame("iframeRightId")
    switch_selected_frame(browser)


def close_detail_page(browser):
    browser.switch_to.default_content()
    browser.find_element_by_css_selector(
        "#tabs > div.tabs-header > div.tabs-wrap > ul > li.tabs-last > a.tabs-close").click()


def switch_selected_frame(browser):
    browser.switch_to.default_content()
    panals = browser.find_elements_by_css_selector(".tabs-panels .panel")
    for panal in panals:
        if "<div class=\"panel\" style=\"display: block; width:" in panal.get_attribute("outerHTML"):
            iframe = panal.find_element_by_tag_name("iframe")
            browser.switch_to.frame(iframe)


def get_text(browser, selector):
    try:
        return browser.find_element_by_css_selector(selector).text
    except Exception as e:
        if "no such element" in str(e):
            return ""
        return "获取失败" + str(e)


def do_detail_page(browser):
    # print(browser.page_source)
    # 加载电话号码
    browser.find_element_by_css_selector(
        "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(2) > div.showdetailsinfo > span").click()

    i = 0
    while i < 10:
        i = i + 1
        time.sleep(0.002)
        phone = get_text(browser,
                         "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(2) > div.showdetailsinfo > span")
        if "点击显示号码" != phone:
            break

    userinfo = {}
    userinfo["姓名"] = get_text(browser,
                              "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(1) > div.showdetailsinfo > textarea")
    userinfo["手机号"] = get_text(browser,
                               "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(2) > div.showdetailsinfo > span")
    userinfo["来源"] = get_text(browser,
                              "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(3) > div.showdetailsinfo")
    userinfo["申请城市"] = get_text(browser,
                                "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(4) > div.showdetailsinfo")
    userinfo["学历"] = get_text(browser,
                              "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(5) > div.showdetailsinfo")
    userinfo["户籍"] = get_text(browser,
                              "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(6) > div.showdetailsinfo")
    userinfo["年龄"] = get_text(browser,
                              "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(7) > div.showdetailsinfo")
    userinfo["婚姻"] = get_text(browser,
                              "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(8) > div.showdetailsinfo")
    userinfo["身份证"] = get_text(browser,
                               "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(9) > div.showdetailsinfo")
    userinfo["性别"] = get_text(browser,
                              "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(10) > div.showdetailsinfo")
    userinfo["毕业院校"] = get_text(browser,
                                "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(11) > div.showdetailsinfo")
    userinfo["客户公司"] = get_text(browser,
                                "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(12) > div.showdetailsinfo")
    userinfo["申请额度"] = get_text(browser,
                                "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(13) > div.showdetailsinfo")
    userinfo["来源类型"] = get_text(browser,
                                "#infomation-info10 > div:nth-child(1) > ul > li:nth-child(14) > div.showdetailsinfo")
    userinfo["名下房产情况"] = get_text(browser, "#infomation-info10 > div:nth-child(2) > ul > li > div.showdetailsinfo")
    userinfo["名下车产情况"] = get_text(browser,
                                  "#infomation-info10 > div:nth-child(3) > ul > li:nth-child(1) > div.showdetailsinfo")
    userinfo["汽车类型"] = get_text(browser,
                                "#infomation-info10 > div:nth-child(3) > ul > li:nth-child(2) > div.showdetailsinfo")
    userinfo["汽车归属地"] = get_text(browser,
                                 "#infomation-info10 > div:nth-child(3) > ul > li:nth-child(3) > div.showdetailsname")
    userinfo["汽车使用年限"] = get_text(browser,
                                  "#infomation-info10 > div:nth-child(3) > ul > li:nth-child(4) > div.showdetailsinfo")
    userinfo["汽车价值"] = get_text(browser,
                                "#infomation-info10 > div:nth-child(3) > ul > li:nth-child(5) > div.showdetailsinfo")
    userinfo["车险公司"] = get_text(browser,
                                "#infomation-info10 > div:nth-child(3) > ul > li:nth-child(6) > div.showdetailsinfo")
    userinfo["当前保单缴费次数"] = get_text(browser,
                                    "#infomation-info10 > div:nth-child(3) > ul > li:nth-child(7) > div.showdetailsinfo")
    userinfo["车辆状态"] = get_text(browser,
                                "#infomation-info10 > div:nth-child(3) > ul > li:nth-child(8) > div.showdetailsinfo")
    userinfo["BD有无保单"] = get_text(browser,
                                  "#infomation-info10 > div:nth-child(4) > ul > li:nth-child(1) > div.showdetailsinfo")
    userinfo["保单公司"] = get_text(browser,
                                "#infomation-info10 > div:nth-child(4) > ul > li:nth-child(2) > div.showdetailsinfo")
    userinfo["年缴费额"] = get_text(browser,
                                "#infomation-info10 > div:nth-child(4) > ul > li:nth-child(3) > div.showdetailsinfo")
    userinfo["保单状态"] = get_text(browser,
                                "#infomation-info10 > div:nth-child(4) > ul > li:nth-child(4) > div.showdetailsinfo")
    userinfo["已缴次数"] = get_text(browser,
                                "#infomation-info10 > div:nth-child(4) > ul > li:nth-child(5) > div.showdetailsinfo")
    userinfo["两年内最严重的逾期"] = get_text(browser,
                                     "#infomation-info10 > div:nth-child(5) > ul > li:nth-child(1) > div.showdetailsinfo")
    userinfo["一年内最严重的逾期"] = get_text(browser,
                                     "#infomation-info10 > div:nth-child(5) > ul > li:nth-child(2) > div.showdetailsinfo")
    userinfo["半年内最严重的逾期"] = get_text(browser,
                                     "#infomation-info10 > div:nth-child(5) > ul > li:nth-child(3) > div.showdetailsinfo")
    userinfo["微粒贷额度"] = get_text(browser,
                                 "#infomation-info10 > div:nth-child(5) > ul > li:nth-child(4) > div.showdetailsinfo")
    userinfo["芝麻信用分"] = get_text(browser,
                                 "#infomation-info10 > div:nth-child(5) > ul > li:nth-child(5) > div.showdetailsinfo")
    userinfo["信用卡情况"] = get_text(browser,
                                 "#infomation-info10 > div:nth-child(6) > ul > li:nth-child(1) > div.showdetailsinfo")
    userinfo["贷记卡总额总额度"] = get_text(browser,
                                    "#infomation-info10 > div:nth-child(6) > ul > li:nth-child(2) > div.showdetailsinfo")
    userinfo["贷记卡当前使用额度"] = get_text(browser,
                                     "#infomation-info10 > div:nth-child(6) > ul > li:nth-child(3) > div.showdetailsinfo")
    userinfo["贷记卡近半年使用额度"] = get_text(browser,
                                      "#infomation-info10 > div:nth-child(6) > ul > li:nth-child(4) > div.showdetailsinfo")
    userinfo["单张贷记卡最高负债率"] = get_text(browser,
                                      "#infomation-info10 > div:nth-child(6) > ul > li:nth-child(5) > div.showdetailsinfo")
    userinfo["有无信用贷款"] = get_text(browser,
                                  "#infomation-info10 > div:nth-child(6) > ul > li:nth-child(6) > div.showdetailsinfo")
    # userinfo["客户星级"]=browser.find_elements_by_css_selector("#stars").text
    # userinfo["客户状态"]=browser.find_elements_by_css_selector("#status").text
    # userinfo["顾问记录"]=browser.find_elements_by_css_selector("#hisul").text
    # userinfo[""]=browser.find_elements_by_css_selector("").text
    # userinfo[""]=browser.find_elements_by_css_selector("").text
    # userinfo[""]=browser.find_elements_by_css_selector("").text
    # userinfo[""]=browser.find_elements_by_css_selector("").text
    return userinfo


def execute():
    # https://www.sudai9.com/manage/kefu_clients/index.html
    # 先打开我的客户
    browser = seleniumutil.chrome_driver_remote()

    # browser = seleniumutil.chrome_driver_remote()
    # browser.switch_to.default_content()
    # browser.find_element_by_css_selector("#_easyui_tree_1 > span.tree-title").click()
    # browser.switch_to.frame("iframeRightId")

    data_csvfile = open(config.PATH_DESKTOP + "/再分配客户.csv", "a", newline='', encoding='utf-8')
    data_writer = csv.writer(data_csvfile)
    errorfile = open(config.PATH_DESKTOP + "error.txt", "w")
    errorfile.writelines("开始了")
    headers = None

    for i in range(1, 100):
        to_search_page(browser)
        trs = browser.find_elements_by_css_selector("#lk_content > table > tbody > tr")

        for tr in trs:
            try:
                to_search_page(browser)
                tr_html = tr.get_attribute("innerHTML")
                if "Open" not in tr_html:
                    continue
                tr.find_elements_by_css_selector("td")[2].click()
                to_detail_page(browser)
                r = do_detail_page(browser)
                if not headers:
                    headers = r.keys()
                    data_writer.writerow(headers)
                data_writer.writerow([r[h] for h in headers])

                close_detail_page(browser)
            except Exception as e:
                errorfile.writelines("----------------------------------------------")
                errorfile.writelines(tr.text)
                errorfile.writelines(str(e))
        if browser.find_elements_by_css_selector("#pagination > a.next"):
            browser.find_element_by_css_selector("#pagination > a.next").click()



    data_csvfile.close()
    errorfile.close()


if __name__ == "__main__":
    execute()
