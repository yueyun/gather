# 分sheet记录种类 ,新建文件&写入原文件&不要索引

import pandas as pd

# hua = pd.read_excel(r"C:\Users\Administrator\Desktop\20191117.xlsx", sheet_name='花花')
#
# class_list = list(hua['种类'].drop_duplicates())

# 一张表3 sheet

# writer = pd.ExcelWriter(r"C:\Users\Administrator\Desktop\20191116.xlsx", index=False)
#
# for i in class_list:
#     print(hua['种类'])
#     print(i)
#     hua1 = hua[hua['种类'] == i]
#
#     print(type(hua1))
#     print(hua1)
#     hua1.to_excel(writer, i, index=False)  # index=False ,在这里控制是否显示索引
#
#     print("****************")
# writer.save()
# writer.close()

# 3张表

# writer = pd.ExcelWriter(r"C:\Users\Administrator\Desktop\fafa.xlsx", index=False)

# for i in class_list:
#     writer = pd.ExcelWriter(r"C:\Users\Administrator\Desktop\20191117_" + i + ".xlsx", index=False)  # A/B/C三张表
#     # writer = pd.ExcelWriter(r"C:\Users\Administrator\Desktop\20191117_哈哈哈.xlsx", index=False)  # 只输出C种类
#
#     print(hua['种类'])
#     print(i)
#     hua1 = hua[hua['种类'] == i]
#
#     print(type(hua1))
#     print(hua1)
#     hua1.to_excel(writer, i, index=False)
#
#     writer.save()
#     writer.close()
#     print("****************")


# 切片
# writer = pd.ExcelWriter(r"C:\Users\Administrator\Desktop\20191117_切片.xlsx", index=False)
#
# size = hua.shape[0]
# # print(hua.shape)
# # print(size)
# page = int(size / 3) + 1
# print(page)
# # for i in range(0, page):
# for i in range(page):
#     # print(hua['种类'])
#     print(i)
#     hua1 = hua[i * 3:(i + 1) * 3]
#     if len(hua1) <= 0:
#         break
#     # print(type(hua1))
#     print(hua1)
#     hua1.to_excel(writer, "a" + str(i), index=False)
#
#     print("****************")
# writer.save()
# writer.close()

# 合并文件
writer = pd.ExcelWriter(r"C:\Users\Administrator\Desktop\合并.xlsx", sheet_name='aa', index=False)

aa = pd.read_excel(r"C:\Users\Administrator\Desktop\A.xlsx", sheet_name='A')
bb = pd.read_excel(r"C:\Users\Administrator\Desktop\B.xlsx", sheet_name='B')
cc = pd.read_excel(r"C:\Users\Administrator\Desktop\C.xlsx", sheet_name='C')
dd = pd.read_excel(r"C:\Users\Administrator\Desktop\D.xlsx", sheet_name='D')
aa = aa.append(bb)
aa = aa.append(cc)
aa = aa.append(dd)
print(aa)
print(type(aa))

aa.to_excel(writer, index=False)
writer.save()
writer.close()
