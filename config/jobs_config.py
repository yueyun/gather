import logging
import time

import datetime
import schedule


# schedule.every(10).minutes.do(jobs_config.heartbeat)


def start():
    if schedule.jobs.__len__() <= 0:
        logging.info("没有配置定时任务!!!")
        return
    while True:
        schedule.run_pending()
        time.sleep(1)  # 每隔一秒验证一下有没有任务要跑
