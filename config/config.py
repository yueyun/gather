# -*- coding: utf-8 -*-

import os

# 桌面路径,一个默认路径
PATH_DESKTOP = r"E:\gather_tmp"

if not os.path.isdir(PATH_DESKTOP):
    os.makedirs(PATH_DESKTOP)

# 默认编码
DEFAULT_CHARSET = "UTF-8"

# 个人邮箱
MYSELF = '1115679859@qq.com'

# 默认邮件发件人
emaiil_smtpsever = 'smtp.qq.com'
email_port = 465
email_sender = '1115679859@qq.com'
email_sender_pwd = "pcumrbzplodvgijc"


gd_app_key = "fb62258b4ae792fbc38511945dc250e1"

lianjia_sources = [
    ["https://sh.lianjia.com/ershoufang/pudong/pg%s", "浦东", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/minhang/pg%s", "闵行", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/baoshan/pg%s", "宝山", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/xuhui/pg%s", "徐汇", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/putuo/pg%s", "普陀", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/yangpu/pg%s", "杨浦", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/changning/pg%s", "长宁", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/songjiang/pg%s", "松江", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/jiading/pg%s", "嘉定", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/huangpu/pg%s", "黄浦", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/jingan/pg%s", "静安", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/zhabei/pg%s", "闸北", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/hongkou/pg%s", "虹口", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/qingpu/pg%s", "青浦", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/fengxian/pg%s", "奉贤", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/jinshan/pg%s", "金山", "上海", "sh"],
    ["https://sh.lianjia.com/ershoufang/chongming/pg%s", "崇明", "上海", "sh"],
    ["https://wh.lianjia.com/ershoufang/jiangan/pg%s", "江岸", "武汉", "wh"],
    ["https://wh.lianjia.com/ershoufang/jianghan/pg%s", "江汉", "武汉", "wh"],
    ["https://wh.lianjia.com/ershoufang/qiaokou/pg%s", "硚口", "武汉", "wh"],
    ["https://wh.lianjia.com/ershoufang/dongxihu/pg%s", "东西湖", "武汉", "wh"],
    ["https://wh.lianjia.com/ershoufang/wuchang/pg%s", "武昌", "武汉", "wh"],
    ["https://wh.lianjia.com/ershoufang/qingshan/pg%s", "青山", "武汉", "wh"],
    ["https://wh.lianjia.com/ershoufang/hongshan/pg%s", "洪山", "武汉", "wh"],
    ["https://wh.lianjia.com/ershoufang/hanyang/pg%s", "汉阳", "武汉", "wh"],
    ["https://wh.lianjia.com/ershoufang/donghugaoxin/pg%s", "东湖高新", "武汉", "wh"],
    ["https://wh.lianjia.com/ershoufang/jiangxia/pg%s", "江夏", "武汉", "wh"],
    ["https://wh.lianjia.com/ershoufang/caidian/pg%s", "蔡甸", "武汉", "wh"],
    ["https://wh.lianjia.com/ershoufang/huangbei/pg%s", "黄陂", "武汉", "wh"],
    ["https://wh.lianjia.com/ershoufang/xinzhou/pg%s", "新洲", "武汉", "wh"],
    ["https://wh.lianjia.com/ershoufang/zhuankoukaifaqu/pg%s", "沌口开发区", "武汉", "wh"]]
