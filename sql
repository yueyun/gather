

drop table T_房源;
CREATE TEMPORARY TABLE T_房源 SELECT *
,replace(面积,'平米','') '面积c',总价/replace(面积,'平米','') '单价c'
,SUBSTRING(挂牌时间,1,7) '月份c',SUBSTRING(挂牌时间,1,4) '年份c',SUBSTRING(挂牌时间,1,10) '日c'
FROM 房源 where 挂牌时间>str_to_date('2018-01-01', '%Y-%m-%d') order by 挂牌时间 desc;

select * from T_房源 limit 1000;

--每月上新数量
select 月份c,count(1) from T_房源 group by 月份c;
--每日上新数量
select 日c,count(1) from T_房源 where 年份c='2019' group by 日c ;

--区域均价
select * from (
select 所在区域,AVG(单价c) '单价',count(1) '上架数',min(单价c) 最低价 from T_房源
where 所在城市 like '%武汉%'
group by 所在区域
)tt order by 单价 asc;

--锚定观察特定区域
select 月份c,AVG(单价c) '单价',count(1) '上架数',min(单价c) 最低价 from T_房源
where 所在区域 like '%洪山%' group by 月份c;

--查询单价区间
select 挂牌时间,单价c,面积c,总价,户型,小区名称,所在区域,产权年限,梯户比例 from T_房源 tt
where  年份c='2019' and 面积c>40 and 所在区域 like '%南翔%' order by 单价c asc


select * from T_房源 where 小区名称='海伦堡爱ME城市'  and 年份c='2019'
select * from T_房源 where 所在区域 like '%崇明%'  and 年份c='2019'