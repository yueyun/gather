import logging
import time

from requests_html import HTMLSession

from common.request import requestutil
from common.utils import timeutil
from config import config

logger = logging.getLogger('searchorg.searcher')


def getFirstrespNodeText(r, selecter):
    try:
        node = r.html.find(selecter)[0]
        full_text = node.full_text
        full_text = full_text.strip("\n").strip(" ").strip("\n")
        return full_text
    except Exception:
        return ""


def getFirstrespNodeText2(r, selecter):
    try:
        node = r.find(selecter)[0]
        full_text = node.full_text
        full_text = full_text.strip("\n").strip(" ").strip("\n")
        return full_text
    except Exception:
        return ""


class searcher_tianyancha():
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Encoding': 'gzip,deflate,sdch,br',
        'Accept-Language': 'zh-CN,zh;q=0.8',
        'Connection': 'keep-alive',
        'Host': 'www.tianyancha.com',
        'Pragma': 'no-cache',
        'Referer': 'https://www.tianyancha.com',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.104 Safari/537.36 Core/1.53.3538.400 QQBrowser/9.6.12578.400',
    }
    cookies = None
    session = None
    domain = "https://www.tianyancha.com/"

    def __init__(self, cookies):
        if cookies:
            self.cookies = cookies

    def search_detail(self, r):
        url = r["DETAIL_URL"]
        orgname = r["STANDED_NAME"]
        return self._search_item(url, orgname)

    def get_cookies(self):

        # self.cookies["_gat_gtag_UA_123487620_1"]=None
        self.cookies["Hm_lpvt_e92c8d65d92d534b0fc290df538b4758"] = str(int(time.time()))
        return self.cookies

    def get_session(self):
        if not self.session:
            self.session = requestutil.get_session()
            # 初始化以下, 把headers,cookies都放进去
            self.session.get("https://www.tianyancha.com/", headers=self.headers, cookies=self.cookies)
        return self.session

    def _search_list(self, orgname):
        # time.sleep(3)
        url = self.domain + 'search?key=' + orgname
        resp = requestutil.get(url, timeout=10, headers=self.headers, cookies=self.cookies)
        # 验证查询页面是否正确加载
        if not (resp.text.find("为你找到") > 0 and resp.text.find("家公司") > 0):
            if resp.text.find("抱歉，没有找到相关企业") > 0:
                return []
            elif resp.text.find("收不到验证码？试试语音验证") > 0:
                logger.error("登录失效,请浏览器重新登录并推送cookie!!账号:" + self.cookies["current_user"])
                raise Exception("登录失效,请浏览器重新登录并推送cookie!!账号:" + self.cookies["current_user"])
            elif resp.text.find("<center>❤</center>") > 0:
                logger.error("请求需要验证码!!账号:" + self.cookies["current_user"])
                raise Exception("请求需要验证码!!账号:" + self.cookies["current_user"])
            else:
                logger.error("没有正确加载页面,请更换词条查询试试!!" + url)
                logger.error(resp.text)
                raise Exception("没有正确加载页面,请更换词条查询试试!!")
        items = resp.html.find(
            ".sv-search-container .sv-search-company")
        results = []
        index = 0
        now_str = timeutil.datetime_to_str()
        for item in items:
            index += 1

            result = {}

            result["i"] = index
            result["KEYWORD"] = orgname
            title_item = item.find("div.content > div.header > a")[0]
            result["STANDED_NAME"] = title_item.text

            result["TELEPHONE"] = getFirstrespNodeText2(item,
                                                        "span:nth-child(3) > span:nth-child(1)")

            result["EMAIL"] = getFirstrespNodeText2(item,
                                                    "div.content > div.contact.row > div:nth-child(2) > span:nth-child(2)")

            result["FDDBR"] = getFirstrespNodeText2(item,
                                                    "div.content > div.info.row.text-ellipsis > div:nth-child(1) > a")
            result["ZCZB"] = getFirstrespNodeText2(item,
                                                   "div.content > div.info.row.text-ellipsis > div:nth-child(2) > span")
            result["PROVINCE"] = getFirstrespNodeText2(item, ".site")
            result["CREATE_TIME"] = now_str
            result["SOURCE"] = "天眼查"

            result["DETAIL_URL"] = title_item.attrs["href"]

            result["JJZT"] = getFirstrespNodeText2(item, "div.content > div.header > div")

            results.append(result)
        results.sort(key=lambda x: x["i"], reverse=False)
        return results


if __name__ == "__main__":
    cookies = requestutil.cookie_dict_from_str(
        "aliyungf_tc=AQAAABAJ32TLPQ4A9k33dMk4fhS8G1Bm; ssuid=4220356200; csrfToken=pcenZMQMg1ISp7BnnS0HkAx7; TYCID=e9a5a90092fd11e9bdfb51a17a08499c; undefined=e9a5a90092fd11e9bdfb51a17a08499c; jsid=SEM-BAIDU-PP-VI-001065; tyc-user-info=%257B%2522claimEditPoint%2522%253A%25220%2522%252C%2522myAnswerCount%2522%253A%25220%2522%252C%2522myQuestionCount%2522%253A%25220%2522%252C%2522signUp%2522%253A%25220%2522%252C%2522explainPoint%2522%253A%25220%2522%252C%2522privateMessagePointWeb%2522%253A%25220%2522%252C%2522nickname%2522%253A%2522%25E6%2589%25B6%25E8%258B%258F%2522%252C%2522integrity%2522%253A%25220%2525%2522%252C%2522privateMessagePoint%2522%253A%25220%2522%252C%2522state%2522%253A0%252C%2522announcementPoint%2522%253A%25220%2522%252C%2522isClaim%2522%253A%25220%2522%252C%2522vipManager%2522%253A%25220%2522%252C%2522discussCommendCount%2522%253A%25220%2522%252C%2522monitorUnreadCount%2522%253A%25220%2522%252C%2522onum%2522%253A%25220%2522%252C%2522claimPoint%2522%253A%25220%2522%252C%2522token%2522%253A%2522eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMzkxNjg2ODY0OSIsImlhdCI6MTU2MDk5NjcyNywiZXhwIjoxNTkyNTMyNzI3fQ.tX3F8d4-r5SeGuONW9WVJK4zUshZSaYWkBYHvRWa3Fjqi6A3Moi6YmfsM89F5e8LaV7fj8w5413QJcQGDoTRCQ%2522%252C%2522pleaseAnswerCount%2522%253A%25220%2522%252C%2522redPoint%2522%253A%25220%2522%252C%2522bizCardUnread%2522%253A%25220%2522%252C%2522vnum%2522%253A%25220%2522%252C%2522mobile%2522%253A%252213916868649%2522%257D; auth_token=eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMzkxNjg2ODY0OSIsImlhdCI6MTU2MDk5NjcyNywiZXhwIjoxNTkyNTMyNzI3fQ.tX3F8d4-r5SeGuONW9WVJK4zUshZSaYWkBYHvRWa3Fjqi6A3Moi6YmfsM89F5e8LaV7fj8w5413QJcQGDoTRCQ; bannerFlag=undefined; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1560836752,1560995484,1560999149,1561094271; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1561094271; _ga=GA1.2.893090315.1560995484; _gid=GA1.2.2118363815.1560995484; _gat_gtag_UA_123487620_1=1")

    cookies["current_user"] = "test_user"
    searcher = searcher_tianyancha(cookies)

    r = searcher._search_list("四川广运九州通医药有限公司")
    print("********************************")
    print(r)
    print("********************************")

    # r = searcher._search_item("company/3220427651", "四川广运九州通医药有限公司")
    # print("********************************")
    # print(r)
    # print("********************************")
