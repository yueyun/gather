import asyncio
import time

from common.utils import timeutil


def testxiechen():
    print("---------Aa1" + timeutil.datetime_to_ymdhms())

    # async def research(w):
    #     print(w + timeutil.datetime_to_ymdhms())
    #     time.sleep(3)
    #     print(w + timeutil.datetime_to_ymdhms())

    async def research(w):
        print(w + timeutil.datetime_to_ymdhms())
        async def ff(w):
            time.sleep(3)
        await ff(w)
        print(w + timeutil.datetime_to_ymdhms())

    try:
        research('Bb').send(None)
    except StopIteration as e:
        print(e.value)
    print("---------Aa2" + timeutil.datetime_to_ymdhms())
    loop = asyncio.get_event_loop()
    loop.run_until_complete(research('Ee'))
    print("---------Aa3" + timeutil.datetime_to_ymdhms())
    #TODO
    loop.run_until_complete(asyncio.wait([research('Ff'), research('Gg')]))
    print("---------Aa4" + timeutil.datetime_to_ymdhms())