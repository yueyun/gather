# -*- coding: utf-8 -*-
from common.request.extract import *


def nt77():
    if __name__ == "__main__":
        domain = "https://m.77nt.com"
    url = domain + "/99147/page-1.html"
    response = requestutil.get(url)
    response.encoding = 'utf-8'
    pages = items(response, "option", text_pattern="^第.*章.*$", fields=["value"])
    for page in pages:
        response = requestutil.get(domain + page["value"])
        response.encoding = 'utf-8'
        titles = items(response, text_pattern="^.*、第.*章.*$")
        for title in titles:
            print(title)


nt77()
