import csv
import re

from common.db import sqltest
from common.request import requestutil
from common.utils import txtutil
from config import config

contents = txtutil.content(r"C:\Users\wangjian\Desktop\48_Headers.txt")
headers = contents.split("------------------------------------------------------------------")

rr = []
keys = set()
for h in headers:
    if not h:
        continue
    try:
        d = requestutil.cookie_dict_from_str(re.search('Cookie(.*)\n', h).group(1))
        d["User-Agent"] = re.search('User-Agent(.*)\n', h).group(1)
        rr.append(d)
        keys = keys.union(d.keys())
    except:
        print(h)

print(keys)

with open(config.PATH_DESKTOP + '\\333.csv', "w", newline="") as csvfile:
    writer = csv.DictWriter(csvfile,
                            fieldnames=keys)
    writer.writeheader()
    for data in rr:
        writer.writerow(data)
