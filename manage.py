from server import app


@app.route('/')
def index():
    return 'this is start page!'


from others import views

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
