"""

1.安装selenium
Win：pip install selenium
Mac:pip3 install selenium

2.下载webdriver
注：webdriver需要和对应的浏览器版本以及selenium版本对应
版本号在notes.text里面可以找到.

各大浏览器webdriver地址可参见：https://docs.seleniumhq.org/download/
Firefox：https://github.com/mozilla/geckodriver/releases/
Chrome：https://sites.google.com/a/chromium.org/chromedriver/ 或者
http://chromedriver.storage.googleapis.com/index.html
IE：http://selenium-release.storage.googleapis.com/index.html


Selenium-3.141.0

Firefox(上一目录可以找其他浏览器的driver) ：
1）http://npm.taobao.org/mirrors/geckodriver/
2）https://github.com/mozilla/geckodriver/releases

Chrome：http://chromedriver.storage.googleapis.com/index.html
IE：http://selenium-release.storage.googleapis.com/index.html

chrome version	Version-73.0.3683.75
chrome driver	Version-2.46

winlO Edge	Version-10.0.17134.1

Firefox	Version-65.0.2（64位）
geckodriver	Version-0.24.0

le浏览器	Version-3.141.5




3.webdriver安装路径
Win：复制webdriver到Python安装目录下
Mac：复制webdriver到/usr/local/bin目录下
其实随便放, 然后调用时指定driver的路径也可以,
比如:driver = webdriver.Chrome(r"D:\Program Files\Tencent\QQBrowser\chromedriver.exe")

4.调用.


网上看资料, selenium自带了firefox?
"""
import time

import os

import sys
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from bs4 import BeautifulSoup

current_path = os.path.realpath(__file__)
current_path = os.path.dirname(current_path) + os.sep


def qq_driver():
    driverOptions = webdriver.ChromeOptions()
    # 不加载图片
    # prefs = {"profile.managed_default_content_settings.images": 2}
    # prefs.add_experimental_option("prefs", prefs)

    # 打开chrome://version/ 可以看到配置文件路径,个人资料,去掉最后面的default
    driverOptions.add_argument(r"user-data-dir=C:\Users\wangjian\AppData\Local\Tencent\QQBrowser\User Data")
    driver = webdriver.Chrome(current_path + r"chromedriver", 0, driverOptions)
    return driver


def chrome_driver():
    driverOptions = webdriver.ChromeOptions()
    # 不加载图片
    # prefs = {"profile.managed_default_content_settings.images": 2}
    # prefs.add_experimental_option("prefs", prefs)

    # 打开chrome://version/ 可以看到配置文件路径,个人资料,去掉最后面的default
    driverOptions.add_argument(r"C:\Users\Administrator\AppData\Local\Google\Chrome\User Data1")
    driver = webdriver.Chrome(current_path + r"chromedriver", 0, driverOptions)
    return driver


def chrome_driver_remote():
    # https://www.cnblogs.com/lovealways/p/9813059.html
    # 利用Chrome DevTools协议。它允许客户检查和调试Chrome浏览器。
    # chrome.exe --remote-debugging-port=9222 --user-data-dir="C:\selenum\AutomationProfile"
    # 然后可以用selenium连上这个浏览器. 而不是每次新打开一个.
    driverOptions = webdriver.ChromeOptions()

    # prefs = {"profile.managed_default_content_settings.images": 2, 'permissions.default.stylesheet': 2}
    # driverOptions.add_experimental_option("prefs", prefs)

    # driverOptions.add_argument('blink-settings=imagesEnabled=false')  # 不加载图片, 提升速度

    driverOptions.add_experimental_option("debuggerAddress", "127.0.0.1:9222")
    chrome_driver = current_path + "chromedriver.exe"
    driver = webdriver.Chrome(chrome_driver, chrome_options=driverOptions)
    return driver

def chrome_driver_remote9223():
    # https://www.cnblogs.com/lovealways/p/9813059.html
    # 利用Chrome DevTools协议。它允许客户检查和调试Chrome浏览器。
    # chrome.exe --remote-debugging-port=9222 --user-data-dir="C:\selenum\AutomationProfile"
    # 然后可以用selenium连上这个浏览器. 而不是每次新打开一个.
    driverOptions = webdriver.ChromeOptions()

    # prefs = {"profile.managed_default_content_settings.images": 2, 'permissions.default.stylesheet': 2}
    # driverOptions.add_experimental_option("prefs", prefs)

    # driverOptions.add_argument('blink-settings=imagesEnabled=false')  # 不加载图片, 提升速度

    driverOptions.add_experimental_option("debuggerAddress", "127.0.0.1:9223")
    chrome_driver = current_path + "chromedriver.exe"
    driver = webdriver.Chrome(chrome_driver, chrome_options=driverOptions)
    return driver

def chrome_driver_remote9224():
    # https://www.cnblogs.com/lovealways/p/9813059.html
    # 利用Chrome DevTools协议。它允许客户检查和调试Chrome浏览器。
    # chrome.exe --remote-debugging-port=9222 --user-data-dir="C:\selenum\AutomationProfile"
    # 然后可以用selenium连上这个浏览器. 而不是每次新打开一个.
    driverOptions = webdriver.ChromeOptions()

    # prefs = {"profile.managed_default_content_settings.images": 2, 'permissions.default.stylesheet': 2}
    # driverOptions.add_experimental_option("prefs", prefs)

    # driverOptions.add_argument('blink-settings=imagesEnabled=false')  # 不加载图片, 提升速度

    driverOptions.add_experimental_option("debuggerAddress", "127.0.0.1:9223")
    chrome_driver = current_path + "chromedriver.exe"
    driver = webdriver.Chrome(chrome_driver, chrome_options=driverOptions)
    return driver

def chrome_driver_clean():
    # driver = webdriver.Chrome()

    driverOptions = webdriver.ChromeOptions()

    # prefs = {"profile.managed_default_content_settings.images": 2, 'permissions.default.stylesheet': 2}
    # driverOptions.add_experimental_option("prefs", prefs)
    driverOptions.add_argument('--disable-gpu')  # 谷歌文档提到需要加上这个属性来规避bug
    # driverOptions.add_argument('--headless')
    # driverOptions.add_argument('blink-settings=imagesEnabled=false')  # 不加载图片, 提升速度

    # driverOptions.add_experimental_option("debuggerAddress", "127.0.0.1:9222")
    chrome_driver = current_path + "chromedriver.exe"
    driver = webdriver.Chrome(chrome_driver, chrome_options=driverOptions)

    return driver


def firefox_driver():
    # 配置文件地址
    # 配置文件地址名 = r'配置路径'
    # 配置路径的查看方式：在火狐浏览器点击？--->（帮助）故障排除信息--->显示文件夹  的那个路径
    profile_dir = r'C:\Users\wangjian\AppData\Roaming\Mozilla\Firefox\Profiles\relhb9bn.default-1506678444564'
    # 加载配置文件   格式：配置文件名 = webdriver.FirefoxProfile(配置文件地址名)
    conf_profile = webdriver.FirefoxProfile(profile_dir)
    # 启动浏览器配置文件  格式：对象名 = webdriver.Firefox(配置文件名)
    driver = webdriver.Firefox(conf_profile)
    return driver


def firefox_driver_clean():
    driver = webdriver.Firefox()
    return driver


######################################################

def cookie_dict_from_str(cookie_strs):
    cookie_str_list = cookie_strs.split(";")
    cookies_dict = []
    for cookie_str in cookie_str_list:
        if not cookie_str.strip():
            continue
        aa = cookie_str.split("=")
        if len(aa) < 2:
            v = ""
        else:
            v = aa[1].strip(" ")
        cookie_dict = {}
        cookie_dict["name"] = aa[0].strip(" ")
        cookie_dict["value"] = v
        cookies_dict.append(cookie_dict)
    return cookies_dict


def test_remote():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')  # 解决DevToolsActivePort文件不存在的报错
    # chrome_options.add_argument('window-size=1920x3000')  # 指定浏览器分辨率
    chrome_options.add_argument('--disable-gpu')  # 谷歌文档提到需要加上这个属性来规避bug
    chrome_options.add_argument('--hide-scrollbars')  # 隐藏滚动条, 应对一些特殊页面
    chrome_options.add_argument('blink-settings=imagesEnabled=false')  # 不加载图片, 提升速度
    # chrome_options.add_argument('--headless')  #浏览器不提供可视化页面. linux下如果系统不支持可视化不加这条会启动失败
    # chrome_options.binary_location = r"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" #手动指定本机电脑使用的浏览器位置

    driver = webdriver.remote.webdriver.WebDriver(
        command_executor="http://192.168.3.100:5000/wd/hub"
        #    , desired_capabilities=DesiredCapabilities.CHROME)
        , desired_capabilities=chrome_options.to_capabilities())

    return driver


def getbs(url):
    browser = chrome_driver()
    browser.get(url)
    soup = BeautifulSoup(browser.page_source, 'html.parser')
    return soup


def test_bs():
    soup = getbs("https://prism.eyee.com/positionnumber/")
    # 确定要抓取的元素位置
    trs = soup.select(
        '#tabletest > tbody > tr > td:nth-of-type(1)')
    for tr in trs:
        print(tr.text)


if __name__ == "__main__":
    browser = chrome_driver_remote()

    url = "https://www.sudai9.com/manage/index/index.html"
    browser.get(url)
    pagebtns = browser.find_elements_by_css_selector("#ajaxpage")
    pagebtns[2].click()
    print(f"browser text = {browser.page_source}")
