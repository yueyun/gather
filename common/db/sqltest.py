import inspect

import re


def test_print_dicts(dts):
    print("**************************", end="\t")
    print("变量:", end="\t")
    for line in inspect.getframeinfo(inspect.currentframe().f_back)[3]:
        m = re.search(r'\btest_print_dicts\s*\(\s*([A-Za-z_][A-Za-z0-9_]*)\s*\)', line)
        if m:
            v_name = m.group(1)
            print(v_name)
    if dts and len(dts) == 0:
        print("没有数据!")
        return

    headers = [k for k in dts[0].keys()]
    print("\t".join(headers))
    print("--------------------------")
    for dt in dts:
        for h in headers:
            print(dt.get(h, "NULL"), end="\t")
        print("")
    print("==========================\n")


def test_print_dict(dt):
    print("**************************", end="\t")
    print("变量:", end="\t")
    for line in inspect.getframeinfo(inspect.currentframe().f_back)[3]:
        m = re.search(r'\btest_print_dict\s*\(\s*([A-Za-z_][A-Za-z0-9_]*)\s*\)', line)
        if m:
            v_name = m.group(1)
            print(v_name)
    headers = [k for k in dt.keys()]
    print("\t".join(headers))
    for h in headers:
        print(dt.get(h, "NULL"), end="\t")

    print("\n==========================\n")


if __name__ == "__main__":
    dtsaa = [{'name': 'ArYe', 'age': 15, 'height': None, 'bb': "cc"}
        , {'name': 'asdf', 'age': 13, 'height': "33", 'bb': "asdf"}]
    test_print_dicts(dtsaa)
    dbb = {'name': 'ArYe', 'age': 15, 'height': None, 'bb': "cc"}
    test_print_dict(dbb)
