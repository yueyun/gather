def dict_to_insert(table_name, dt):
    sql = "insert into %s (%s) values (%s) "
    ls = [(k, dt[k]) for k in dt if dt[k]]
    fields = ','.join([i[0] for i in ls])
    values = ','.join(["%r" % i[1] for i in ls])
    return sql % (table_name, fields, values)


def dict_to_update(table_name, dt):
    return "update %s set %s " % (table_name, _dict_to_set(dt))


def _dict_to_set(dt):
    sql = ""
    for k, v in dt.items():
        sql += "%s=%r," % (k, v)
    return sql

class join_dict():
    def __init__(self,key):
        self.collect_result={}
        self.key=key

    def join(self,dt):
        pass

if __name__ == "__main__":
    tb = 'student'
    dt = {'name': 'ArYe', 'age': 15, 'height': None, 'bb': "cc"}
    print(dict_to_insert(tb, dt))
    print(dict_to_update(tb, dt))
