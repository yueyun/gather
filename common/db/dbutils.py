#!/usr/bin/env python3
# coding: utf-8
import logging

from common.db import mysqlutil, mssqlutil, oracleutil
from common.db.dbconfigs import *

def execute(sql, dbconfig=None, dictionary=False, connection=None, autocommit=True):
    if not dbconfig:
        dbconfig = db_default

    # mysql
    if "host" in dbconfig.keys():
        return mysqlutil.execute(sql, dbconfig, dictionary, connection, autocommit)

    # sqlserver
    if "database" in dbconfig.keys():
        return mssqlutil.execute(sql, dbconfig, dictionary, connection, autocommit)

    # oracle
    try:
        return oracleutil.execute(sql, dbconfig, dictionary, connection, autocommit)
    except Exception as e:
        logging.info("异常SQL:" + sql)
        print("异常SQL:" + sql)
        raise e


def _connection(dbconfig=None):
    if not dbconfig:
        dbconfig = db_default

    # mysql
    if "host" in dbconfig.keys():
        return mysqlutil._connection(dbconfig)

    # sqlserver
    if "database" in dbconfig.keys():
        return mssqlutil._connection(dbconfig)

    # oracle
    return oracleutil._connection(dbconfig)
