import pickle

import os

from config import config


def save(obj, filename, path=config.PATH_DESKTOP):
    filepath = path + os.sep + filename + ".pkl"
    with open(filepath, 'wb') as f:
        pickle.dump(obj, f)


def load(filename, path=config.PATH_DESKTOP):
    filepath = path + os.sep + filename + ".pkl"
    with open(filepath, 'rb') as f:
        obj = pickle.load(f)
        return obj


if __name__ == "__main__":
    # d = {"aaa": 111}
    # save(d, "111")
    b = load("111")
    print(b)
