def content(path):
    f = open(path, 'r', encoding="utf-8")
    content = f.read()
    f.close()
    return content

