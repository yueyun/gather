from concurrent.futures import ThreadPoolExecutor, as_completed
import time

'''
定义了一个公用的线程池
'''
executor = ThreadPoolExecutor(max_workers=5)


def addJob(fn, *args, **kwargs):
    '''
    submit方法将任务加入一个队列中. 这个队列的加入是阻塞的.好像没有看到容量?
    :param fn:
    :param args:
    :param kwargs:
    :return:
    '''
    executor.submit(fn, *args, **kwargs)


# 示例
if __name__ == '__main__':
    def get_html(times):
        time.sleep(times)
        print("get page {}s finished".format(times))
        return times


    urls = [3, 2, 4]  # 并不是真的url
    all_task = [executor.submit(get_html, (url)) for url in urls]

    for future in as_completed(all_task):
        data = future.result()
        print("in main: get page {}s success".format(data))
