import traceback

from requests_html import HTMLSession
from tomorrow import threads
from common.db import dbutils, mysqlutil
from common.request import requestutil
from common.utils import timeutil
from config import config


def getFirstrespNodeText(r, selecter):
    try:
        node = r.html.find(selecter)[0]
        full_text = node.full_text

        node_expect = r.html.find(selecter + " .label")
        if node_expect:
            lab_text = node_expect[0].full_text
            full_text = full_text.lstrip(lab_text)
        return full_text
    except Exception:
        return ""


@threads(10)
def getXiaoquDetail(code, url):
    try:

        resp = requestutil.get(url)

        建筑年代 = getFirstrespNodeText(resp,
                                    "body > div.xiaoquOverview > div.xiaoquDescribe.fr > div.xiaoquInfo > div:nth-child(1) > span.xiaoquInfoContent")
        建筑类型 = getFirstrespNodeText(resp,
                                    "body > div.xiaoquOverview > div.xiaoquDescribe.fr > div.xiaoquInfo > div:nth-child(2) > span.xiaoquInfoContent")
        物业费用 = getFirstrespNodeText(resp,
                                    "body > div.xiaoquOverview > div.xiaoquDescribe.fr > div.xiaoquInfo > div:nth-child(3) > span.xiaoquInfoContent")
        物业公司 = getFirstrespNodeText(resp,
                                    "body > div.xiaoquOverview > div.xiaoquDescribe.fr > div.xiaoquInfo > div:nth-child(4) > span.xiaoquInfoContent")
        开发商 = getFirstrespNodeText(resp,
                                   "body > div.xiaoquOverview > div.xiaoquDescribe.fr > div.xiaoquInfo > div:nth-child(5) > span.xiaoquInfoContent")
        楼栋总数 = getFirstrespNodeText(resp,
                                    "body > div.xiaoquOverview > div.xiaoquDescribe.fr > div.xiaoquInfo > div:nth-child(6) > span.xiaoquInfoContent")
        房屋总数 = getFirstrespNodeText(resp,
                                    "body > div.xiaoquOverview > div.xiaoquDescribe.fr > div.xiaoquInfo > div:nth-child(7) > span.xiaoquInfoContent")
        挂牌均价 = getFirstrespNodeText(resp,
                                    "body > div.xiaoquOverview > div.xiaoquDescribe.fr > div.xiaoquPrice.clear > div > span.xiaoquUnitPrice")

        链家ID = code
        创建时间 = timeutil.datetime_to_ymdhms()

        sql = """
        insert into 小区 (建筑年代, 建筑类型, 物业费用, 物业公司, 开发商  , 楼栋总数, 房屋总数, 挂牌均价, 创建时间, 链家ID ) 
                    values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
                    """ % (建筑年代, 建筑类型, 物业费用, 物业公司, 开发商, 楼栋总数, 房屋总数, 挂牌均价, 创建时间, 链家ID)

        mysqlutil.execute(sql)
    except Exception:
        if requestutil.is_proxy_exception():
            getXiaoquDetail(url)
        else:
            print(traceback.format_exc())


def execute():
    sql = """
    select a.链家小区ID from (
    select DISTINCT 链家小区ID from 房源
    ) a
    LEFT JOIN 小区 b on a.链家小区ID=b.链家ID
    where b.链家ID is null
    """

    链家小区IDs = mysqlutil.execute(sql)

    for 链家小区ID in 链家小区IDs:
        url = "https://sh.lianjia.com/xiaoqu/%s/" % 链家小区ID[0]
        getXiaoquDetail(链家小区ID[0], url)


if __name__ == '__main__':
    execute()
