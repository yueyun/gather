import traceback

from requests_html import HTMLSession
from tomorrow import threads

from common.db import dbutils, sqlgenerater
from common.request import requestutil
from common.utils import timeutil
from config import config


def getFirstrespNodeText(r, selecter):
    try:
        node = r.html.find(selecter)[0]
        full_text = node.full_text

        node_expect = r.html.find(selecter + " .label")
        if node_expect:
            lab_text = node_expect[0].full_text
            full_text = full_text.lstrip(lab_text)
        return full_text
    except Exception:
        return ""


def getHouseList(url, city):
    try:
        resp = requestutil.get(url,timeout=4)

        houses = resp.html.find(".info .title a")
        for house in houses:
            href = house.attrs["href"]
            code = house.attrs["data-housecode"]
            name = house.text
            getHouseDetail(code, city)

    except Exception:
        if requestutil.is_proxy_exception():
            getHouseList(url, city)
        else:
            print(traceback.format_exc())


@threads(3)
def getHouseDetail(code, city):
    exist_house_cnt = dbutils.execute("select count(1) from 房源 where `链家ID`='%s'" % code)[0][0]
    if exist_house_cnt > 0:
        return

    url = "https://" + getCodeByCity(city) + ".lianjia.com/ershoufang/" + code + ".html"
    print(url)
    try:

        resp = requestutil.get(url,timeout=4)
        链家ID = code
        总价 = getFirstrespNodeText(resp, "body > div.overview > div.content > div.price > span.total")
        面积 = getFirstrespNodeText(resp, "body > div.overview > div.content > div.houseInfo > div.area > div.mainInfo")
        户型 = getFirstrespNodeText(resp, "#introduction > div > div > div.base > div.content > ul > li:nth-child(1)")
        所在楼层 = getFirstrespNodeText(resp, "#introduction > div > div > div.base > div.content > ul > li:nth-child(2)")

        所在区域 = getFirstrespNodeText(resp,
                                    "body > div.overview > div.content > div.aroundInfo > div.areaName > span.info")
        户型结构 = getFirstrespNodeText(resp, "#introduction > div > div > div.base > div.content > ul > li:nth-child(4)")
        建筑面积 = getFirstrespNodeText(resp, "#introduction > div > div > div.base > div.content > ul > li:nth-child(3)")
        套内面积 = getFirstrespNodeText(resp, "#introduction > div > div > div.base > div.content > ul > li:nth-child(5)")
        房屋朝向 = getFirstrespNodeText(resp, "#introduction > div > div > div.base > div.content > ul > li:nth-child(7)")
        梯户比例 = getFirstrespNodeText(resp, "#introduction > div > div > div.base > div.content > ul > li:nth-child(10)")
        配备电梯 = getFirstrespNodeText(resp, "#introduction > div > div > div.base > div.content > ul > li:nth-child(11)")
        产权年限 = getFirstrespNodeText(resp, "#introduction > div > div > div.base > div.content > ul > li:nth-child(12)")
        挂牌时间 = getFirstrespNodeText(resp,
                                    "#introduction > div > div > div.transaction > div.content > ul > li:nth-child(1) > span:nth-child(2)")
        上次交易 = getFirstrespNodeText(resp,
                                    "#introduction > div > div > div.transaction > div.content > ul > li:nth-child(3) > span:nth-child(2)")
        交易权属 = getFirstrespNodeText(resp,
                                    "#introduction > div > div > div.transaction > div.content > ul > li:nth-child(2) > span:nth-child(2)")
        房屋用途 = getFirstrespNodeText(resp,
                                    "#introduction > div > div > div.transaction > div.content > ul > li:nth-child(4) > span:nth-child(2)")
        房屋年限 = getFirstrespNodeText(resp,
                                    "#introduction > div > div > div.transaction > div.content > ul > li:nth-child(5) > span:nth-child(2)")
        产权所属 = getFirstrespNodeText(resp,
                                    "#introduction > div > div > div.transaction > div.content > ul > li:nth-child(6) > span:nth-child(2)")

        建筑面积 = 建筑面积.lstrip("㎡")
        套内面积 = 套内面积.lstrip("㎡")
        所在城市 = city

        经纪人 = ""
        采集时间 = timeutil.datetime_to_ymdhms()

        链家小区ID = ""
        小区名称 = ""

        小区nodes = resp.html.find("body > div.overview > div.content > div.aroundInfo > div.communityName > a.info")
        if len(小区nodes) > 0:
            小区名称 = 小区nodes[0].text
            链家小区ID = 小区nodes[0].attrs["href"]
            链家小区ID = 链家小区ID.replace('xiaoqu', '')
            链家小区ID = 链家小区ID.replace('/', '')

        经纪人nodes = resp.html.find("body > div.overview > div.content > div.brokerInfo.clear > div > div.brokerName > a")
        if len(经纪人nodes) > 0:
            经纪人 = 经纪人nodes[0].text
            经纪人 += "-" + 经纪人nodes[0].attrs["data-el"]

        sql = """
insert into 房源 (链家ID,总价,面积,户型,所在楼层,小区名称,所在区域,户型结构,
            套内面积,房屋朝向,梯户比例,配备电梯,产权年限,挂牌时间,上次交易,交易权属,房屋用途,房屋年限,产权所属,所在城市,经纪人,采集时间,建筑面积,链家小区ID) 
            values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s',
             '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
            """ % (链家ID, 总价, 面积, 户型, 所在楼层, 小区名称, 所在区域, 户型结构,
                   套内面积, 房屋朝向, 梯户比例, 配备电梯, 产权年限, 挂牌时间, 上次交易, 交易权属, 房屋用途, 房屋年限, 产权所属, 所在城市, 经纪人, 采集时间, 建筑面积,
                   链家小区ID)

        # ls = [(k, dt[k]) for k in dt if dt[k]]
        # sql = "insert user (%s) values (%s);" % \
        #       (','.join([i[0] for i in ls]), ','.join(['%r' % i[1] for i in ls]))

        dbutils.execute(sql)
    except Exception:
        if requestutil.is_proxy_exception():
            getHouseDetail(code)
        else:
            print(traceback.format_exc() + url)


def execute():
    for s in config.lianjia_sources:
        for i in range(1, 100):
            u = s[0] % i
            getHouseList(u, s[2] + "-" + s[1])


def getCodeByCity(city):
    if city.__contains__('上海'):
        return 'sh'
    if city.__contains__('武汉'):
        return 'wh'
    raise


if __name__ == '__main__':
    execute()
    # getHouseDetail("107101248011", "上海")
